module cards-app-2.0

go 1.16

require (
	github.com/99designs/gqlgen v0.13.0
	github.com/go-chi/chi v3.3.2+incompatible
	github.com/go-chi/chi/v5 v5.0.2
	github.com/golang/mock v1.5.0
	github.com/mattn/go-sqlite3 v1.14.6
	github.com/stretchr/testify v1.7.0
	github.com/vektah/gqlparser/v2 v2.1.0
)
