package helper

import (
	"math/rand"
	"strings"
	"time"
)

type Deck []string

func NewDeck() Deck {
	d := Deck{"Joker1", "Joker2"}
	cardSuits := []string{"Spades", "Diamonds", "Clubs", "Hearts"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}

	for i := 0; i < 2; i++ {
		for _, values := range cardValues {
			d = append(d, values+" of "+cardSuits[i])
		}
	}

	for i := 2; i < 4; i++ {
		for j := 12; j >= 0; j-- {
			d = append(d, cardValues[j]+" of "+cardSuits[i])
		}
	}

	return d
}

func (d Deck) ToString() string {
	return strings.Join([]string(d), ",")
}

func (d Deck) Shuffle() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}
