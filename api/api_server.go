package api

import (
	"cards-app-2.0/db"
	"github.com/go-chi/chi"
)

var server *apiServer

type apiServer struct {
	db db.CardsDB
	r  *chi.Mux
}

func newAPIServer(db db.CardsDB) *apiServer {
	return &apiServer{db: db, r: chi.NewRouter()}
}
