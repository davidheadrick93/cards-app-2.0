package graph

import (
	"log"
	"testing"
	"time"

	"cards-app-2.0/graph/generated"
	"cards-app-2.0/graph/model"
	mock "cards-app-2.0/mocks"
	"github.com/99designs/gqlgen/client"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

var input = &model.Deck{
	Username:  "DavidHeadrick",
	DeckName:  "TestDeck",
	Deck:      "Joker1,Joker2,Ace of Spades,Two of Spades,Three of Spades,Four of Spades,Five of Spades,Six of Spades,Seven of Spades,Eight of Spades,Nine of Spades,Ten of Spades,Jack of Spades,Queen of Spades,King of Spades,Ace of Diamonds,Two of Diamonds,Three of Diamonds,Four of Diamonds,Five of Diamonds,Six of Diamonds,Seven of Diamonds,Eight of Diamonds,Nine of Diamonds,Ten of Diamonds,Jack of Diamonds,Queen of Diamonds,King of Diamonds,King of Clubs,Queen of Clubs,Jack of Clubs,Ten of Clubs,Nine of Clubs,Eight of Clubs,Seven of Clubs,Six of Clubs,Five of Clubs,Four of Clubs,Three of Clubs,Two of Clubs,Ace of Clubs,King of Hearts,Queen of Hearts,Jack of Hearts,Ten of Hearts,Nine of Hearts,Eight of Hearts,Seven of Hearts,Six of Hearts,Five of Hearts,Four of Hearts,Three of Hearts,Two of Hearts,Ace of Hearts",
	CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
	UpdatedAt: time.Now().Format("2006-01-02 15:04:05"),
}

var shuffled = &model.Deck{
	Username:  "DavidHeadrick",
	DeckName:  "TestDeck",
	Deck:      "Joker1,Joker2,Ace of Spades,Two of Spades,Three of Spades,Four of Spades,Five of Spades,Six of Spades,Seven of Spades,Eight of Spades,Nine of Spades,Ten of Spades,Jack of Spades,Queen of Spades,King of Spades,Ace of Diamonds,Two of Diamonds,Three of Diamonds,Four of Diamonds,Five of Diamonds,Six of Diamonds,Seven of Diamonds,Eight of Diamonds,Nine of Diamonds,Ten of Diamonds,Jack of Diamonds,Queen of Diamonds,King of Diamonds,King of Clubs,Queen of Clubs,Jack of Clubs,Ten of Clubs,Nine of Clubs,Eight of Clubs,Seven of Clubs,Six of Clubs,Five of Clubs,Four of Clubs,Three of Clubs,Two of Clubs,Ace of Clubs,King of Hearts,Queen of Hearts,Jack of Hearts,Ten of Hearts,Nine of Hearts,Eight of Hearts,Seven of Hearts,Six of Hearts,Five of Hearts,Four of Hearts,Three of Hearts,Two of Hearts,Ace of Hearts",
	CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
	UpdatedAt: time.Now().Format("2006-01-02 15:04:05"),
}

func TestCreateDeck(t *testing.T) {

	// build mock
	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Create(input).AnyTimes().Return(nil)

	// build graphql client and server
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: cardsDB},
	}))
	c := client.New(srv)

	// build a container for the response
	var resp struct {
		CreateDeck model.Deck
	}

	// use the client to post the mutation
	c.MustPost(`mutation m($deckName: String!) { 
			createDeck(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp, client.Var("deckName", input.DeckName))

	// assert all the equals
	assert.Equal(t, input.Username, resp.CreateDeck.Username)
	assert.NotEmpty(t, resp.CreateDeck.Deck)
	assert.Equal(t, input.DeckName, resp.CreateDeck.DeckName)
	assert.NotEmpty(t, resp.CreateDeck.UpdatedAt)
	assert.NotEmpty(t, resp.CreateDeck.CreatedAt)

}

func TestGetDeckDetails(t *testing.T) {

	// build mock
	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Create(input).AnyTimes().Return(nil)
	cardsDB.EXPECT().Read(input.DeckName).AnyTimes().Return(input, nil)

	// build graphql client and server
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: cardsDB},
	}))
	c := client.New(srv)

	// build a container for the response
	var resp1 struct {
		CreateDeck model.Deck
	}
	var resp2 struct {
		GetDeckDetails model.Deck
	}

	// use the client to post the mutation
	c.MustPost(`mutation m($deckName: String!) { 
			createDeck(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp1, client.Var("deckName", input.DeckName))

	c.MustPost(`query q($deckName: String!) { 
			getDeckDetails(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp2, client.Var("deckName", input.DeckName))

	// assert all the equals
	assert.Equal(t, input.Username, resp2.GetDeckDetails.Username)
	assert.NotEmpty(t, resp2.GetDeckDetails.Deck)
	assert.Equal(t, input.DeckName, resp2.GetDeckDetails.DeckName)
	assert.NotEmpty(t, resp2.GetDeckDetails.UpdatedAt)
	assert.NotEmpty(t, resp2.GetDeckDetails.CreatedAt)

}

func TestShuffleDeck(t *testing.T) {
	// build mock
	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Create(shuffled).AnyTimes().Return(nil)
	cardsDB.EXPECT().Read(shuffled.DeckName).AnyTimes().Return(shuffled, nil)
	cardsDB.EXPECT().Update(shuffled).AnyTimes().Return(input, nil)

	// build graphql client and server
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: cardsDB},
	}))
	c := client.New(srv)

	// build a container for the response
	var resp1 struct {
		CreateDeck model.Deck
	}
	var resp2 struct {
		ShuffleDeck model.Deck
	}

	// use the client to post the mutation
	c.MustPost(`mutation m($deckName: String!) { 
			createDeck(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp1, client.Var("deckName", shuffled.DeckName))

	c.MustPost(`mutation m($deckName: String!) { 
			shuffleDeck(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp2, client.Var("deckName", shuffled.DeckName))

	log.Printf("shuffled, resp1, resp2: %v | %v | %v", shuffled, resp1, resp2) // debugging

	// assert all the equals
	assert.Equal(t, shuffled.Username, resp2.ShuffleDeck.Username)
	assert.NotEmpty(t, resp2.ShuffleDeck.Deck)
	assert.Equal(t, shuffled.DeckName, resp2.ShuffleDeck.DeckName)
	assert.NotEmpty(t, resp2.ShuffleDeck.UpdatedAt)
	assert.NotEmpty(t, resp2.ShuffleDeck.CreatedAt)
}

func TestDeleteDeck(t *testing.T) {

	// build mock
	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Create(input).AnyTimes().Return(nil)
	cardsDB.EXPECT().Delete(input.Username, input.DeckName).AnyTimes().Return(nil)

	// build graphql client and server
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &Resolver{DB: cardsDB},
	}))
	c := client.New(srv)

	// build a container for the response
	var resp struct {
		CreateDeck model.Deck
	}

	var resp2 struct {
		deleteDeck string
	}

	// use the client to post the mutation
	c.MustPost(`mutation m($deckName: String!) { 
			createDeck(input: $deckName) { 
				username
				deck
				deck_name
				created_at
				updated_at
			} 
		}`, &resp, client.Var("deckName", input.DeckName))

	c.MustPost(`mutation m($deckName: String!) { 
			deleteDeck(input: $deckName) 
		}`, &resp2, client.Var("deckName", input.DeckName))

	// assert all the equals
	assert.Equal(t, input.Username, resp.CreateDeck.Username)
	assert.NotEmpty(t, resp.CreateDeck.Deck)
	assert.Equal(t, input.DeckName, resp.CreateDeck.DeckName)
	assert.NotEmpty(t, resp.CreateDeck.UpdatedAt)
	assert.NotEmpty(t, resp.CreateDeck.CreatedAt)

}
