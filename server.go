package main

import (
	"net/http"
	"os"

	"cards-app-2.0/db"
	"cards-app-2.0/graph"
	"cards-app-2.0/graph/generated"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
)

const defaultDBfile = "./db/deck.db"
const defaultPort = "3333"

func main() {
	dbFile := os.Getenv("DB_FILE")
	if dbFile == "" {
		dbFile = defaultDBfile
	}

	dbConn := db.Connect(dbFile)
	defer dbConn.Close()

	port := os.Getenv("PORT")
	if port == "" {
		port = defaultPort
	}

	r := chi.NewRouter()
	r.Use(middleware.BasicAuth("simple", map[string]string{"DavidH": "PuppyMonkeyBaby"}))
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)

	// r.Mount("/", api.Routes(db.NewCardsConnection(dbConn)))
	srv := handler.NewDefaultServer(generated.NewExecutableSchema(generated.Config{
		Resolvers: &graph.Resolver{DB: db.NewCardsConnection(dbConn)},
	}))
	r.Mount("/", playground.Handler("GraphQL playground", "/query"))
	r.Mount("/query", srv)

	http.ListenAndServe(":"+port, r)

}
