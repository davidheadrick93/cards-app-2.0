package db

import (
	"database/sql"
	"testing"
	"time"

	"cards-app-2.0/graph/model"
	"github.com/stretchr/testify/assert"
)

var testConnection *sql.DB

var deckTest = &model.Deck{
	Username:  "DavidHeadrick",
	DeckName:  "TestDeck",
	Deck:      "Joker1,Joker2,Ace of Spades,Two of Spades,Three of Spades,Four of Spades,Five of Spades,Six of Spades,Seven of Spades,Eight of Spades,Nine of Spades,Ten of Spades,Jack of Spades,Queen of Spades,King of Spades,Ace of Diamonds,Two of Diamonds,Three of Diamonds,Four of Diamonds,Five of Diamonds,Six of Diamonds,Seven of Diamonds,Eight of Diamonds,Nine of Diamonds,Ten of Diamonds,Jack of Diamonds,Queen of Diamonds,King of Diamonds,King of Clubs,Queen of Clubs,Jack of Clubs,Ten of Clubs,Nine of Clubs,Eight of Clubs,Seven of Clubs,Six of Clubs,Five of Clubs,Four of Clubs,Three of Clubs,Two of Clubs,Ace of Clubs,King of Hearts,Queen of Hearts,Jack of Hearts,Ten of Hearts,Nine of Hearts,Eight of Hearts,Seven of Hearts,Six of Hearts,Five of Hearts,Four of Hearts,Three of Hearts,Two of Hearts,Ace of Hearts",
	CreatedAt: time.Now().AddDate(-1, 0, 0).Format("2006-01-02 15:04:05"),
	UpdatedAt: time.Now().AddDate(-1, 0, 0).Format("2006-01-02 15:04:05"),
}

func TestCreate(t *testing.T) {
	testConnection = Connect("../test/test.db")
	defer testConnection.Close()

	clearDB(testConnection)

	testDB := NewCardsConnection(testConnection)

	err := testDB.Create(deckTest)
	assert.Nil(t, err)
}

func TestRead(t *testing.T) {
	testCases := []struct {
		input  *model.Deck
		output *model.Deck
		err    error
	}{
		{
			deckTest,
			deckTest,
			nil,
		},
		{
			&model.Deck{Username: "", DeckName: "", Deck: "", CreatedAt: time.Now().Format("2006-01-02 15:04:05"), UpdatedAt: time.Now().Format("2006-01-02 15:04:05")},
			nil,
			sql.ErrNoRows,
		},
	}

	for _, tc := range testCases {

		testConnection = Connect("../test/test.db")
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewCardsConnection(testConnection)

		if tc.input.Username != "" {
			err := testDB.Create(tc.input)
			assert.Nil(t, err)
		}

		deck, err := testDB.Read(tc.input.DeckName)
		assert.Equal(t, tc.output, deck)
		assert.Equal(t, tc.err, err)
	}
}

func TestUpdate(t *testing.T) {
	testCases := []struct {
		input *model.Deck
		err   error
	}{
		{
			deckTest,
			nil,
		},
		{
			&model.Deck{Username: "", DeckName: "", Deck: "", CreatedAt: time.Now().Format("2006-01-02 15:04:05"), UpdatedAt: time.Now().Format("2006-01-02 15:04:05")},
			sql.ErrNoRows,
		},
	}

	for _, tc := range testCases {
		testConnection = Connect("../test/test.db")
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewCardsConnection(testConnection)

		if tc.input.Username != "" {
			err := testDB.Create(tc.input)
			assert.Nil(t, err)
		}

		deck, err := testDB.Update(tc.input)
		assert.Equal(t, tc.err, err)
		if tc.input.Username != "" {
			assert.Greater(t, deck.UpdatedAt, tc.input.UpdatedAt)
		} else {
			assert.Nil(t, deck)
		}
	}
}

func TestDelete(t *testing.T) {
	testCases := []struct {
		input *model.Deck
		err   error
	}{
		{
			deckTest,
			nil,
		},
	}

	for _, tc := range testCases {
		testConnection = Connect("../test/test.db")
		defer testConnection.Close()

		clearDB(testConnection)

		testDB := NewCardsConnection(testConnection)

		if tc.input.Username != "" {
			err := testDB.Create(deckTest)
			assert.Nil(t, err)
		}

		err := testDB.Delete(deckTest.Username, deckTest.DeckName)
		assert.Equal(t, tc.err, err)
	}
}

func clearDB(db *sql.DB) {
	db.Exec("DELETE from cards")
}
