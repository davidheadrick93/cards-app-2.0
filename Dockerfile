FROM golang:1.16 as builder

WORKDIR /go/src/gitlab.com/davidheadrick93/cards-app-2.0

COPY go.* ./
RUN go mod download
COPY . .


RUN GOOS=linux go build -a -ldflags="-extldflags=-static" -o cards-app-2.0 server.go
RUN adduser                 \
    --disabled-password     \
    --gecos ""              \
    --home "/nonexistent"   \
    --shell "/sbin/nologin" \
    --uid "987654321"       \
    "user"



FROM scratch

COPY --from=builder /go/src/gitlab.com/davidheadrick93/cards-app-2.0/cards-app-2.0 /cards-app-2.0

ENV PORT=3333
ENV DB_FILE /db/deck.db

USER user

COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder --chown=user /go/src/gitlab.com/davidheadrick93/cards-app-2.0/db/deck.db /db/deck.db



EXPOSE ${PORT}



CMD ["/cards-app-2.0"]
