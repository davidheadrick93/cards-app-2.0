package api

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"cards-app-2.0/graph/model"
	mock "cards-app-2.0/mocks"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
)

var deckTest = &model.Deck{
	Username:  "DavidHeadrick",
	DeckName:  "TestDeck",
	Deck:      "Joker1,Joker2,Ace of Spades,Two of Spades,Three of Spades,Four of Spades,Five of Spades,Six of Spades,Seven of Spades,Eight of Spades,Nine of Spades,Ten of Spades,Jack of Spades,Queen of Spades,King of Spades,Ace of Diamonds,Two of Diamonds,Three of Diamonds,Four of Diamonds,Five of Diamonds,Six of Diamonds,Seven of Diamonds,Eight of Diamonds,Nine of Diamonds,Ten of Diamonds,Jack of Diamonds,Queen of Diamonds,King of Diamonds,King of Clubs,Queen of Clubs,Jack of Clubs,Ten of Clubs,Nine of Clubs,Eight of Clubs,Seven of Clubs,Six of Clubs,Five of Clubs,Four of Clubs,Three of Clubs,Two of Clubs,Ace of Clubs,King of Hearts,Queen of Hearts,Jack of Hearts,Ten of Hearts,Nine of Hearts,Eight of Hearts,Seven of Hearts,Six of Hearts,Five of Hearts,Four of Hearts,Three of Hearts,Two of Hearts,Ace of Hearts",
	CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
	UpdatedAt: time.Now().Format("2006-01-02 15:04:05"),
}

func TestSlashHandler(t *testing.T) {

	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)

	server = newAPIServer(cardsDB)
	server.r.Get("/", slashHandler)

	byteDeckTest, err := json.Marshal("")
	assert.NoError(t, err)

	req, err := http.NewRequest("GET", "/", bytes.NewBuffer(byteDeckTest))
	assert.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	server.r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	assert.Equal(t, "Hello There!", rr.Body.String())

}

func TestCardHandler(t *testing.T) {
	tt := []struct {
		input    interface{}
		output   *model.Deck
		response string
		code     int
	}{
		{deckTest, deckTest, `{"status:":"created"}`, http.StatusCreated},
		{"haha, this is a bad test case", &model.Deck{Username: "badtest", DeckName: "bad", Deck: "", CreatedAt: time.Now().Format("2006-01-02 15:04:05"), UpdatedAt: time.Now().Format("2006-01-02 15:04:05")}, `{"status:":"not created"}`, http.StatusBadRequest},
	}

	for _, tc := range tt {
		t.Logf("Now running test with input %s\n", tc.input)

		ctrl := gomock.NewController(t)

		cardsDB := mock.NewMockCardsDB(ctrl)
		cardsDB.EXPECT().Create(tc.input).AnyTimes().Return(nil)

		server = newAPIServer(cardsDB)
		server.r.Post("/cards", cardHandler)

		byteDeckTest, err := json.Marshal(tc.input)
		assert.NoError(t, err)

		req, err := http.NewRequest("POST", "/cards", bytes.NewBuffer(byteDeckTest))
		assert.NoError(t, err)
		req.Header.Set("Content-Type", "application/json")

		rr := httptest.NewRecorder()

		server.r.ServeHTTP(rr, req)
		assert.Equal(t, tc.code, rr.Code)

		assert.Equal(t, tc.response, rr.Body.String())
	}
}

func TestFetchHandler(t *testing.T) {

	tt := []struct {
		input    interface{}
		output   *model.Deck
		response string
		code     int
	}{
		{deckTest, deckTest, `{"status:":"Joker1,Joker2,Ace of Spades,Two of Spades,Three of Spades,Four of Spades,Five of Spades,Six of Spades,Seven of Spades,Eight of Spades,Nine of Spades,Ten of Spades,Jack of Spades,Queen of Spades,King of Spades,Ace of Diamonds,Two of Diamonds,Three of Diamonds,Four of Diamonds,Five of Diamonds,Six of Diamonds,Seven of Diamonds,Eight of Diamonds,Nine of Diamonds,Ten of Diamonds,Jack of Diamonds,Queen of Diamonds,King of Diamonds,King of Clubs,Queen of Clubs,Jack of Clubs,Ten of Clubs,Nine of Clubs,Eight of Clubs,Seven of Clubs,Six of Clubs,Five of Clubs,Four of Clubs,Three of Clubs,Two of Clubs,Ace of Clubs,King of Hearts,Queen of Hearts,Jack of Hearts,Ten of Hearts,Nine of Hearts,Eight of Hearts,Seven of Hearts,Six of Hearts,Five of Hearts,Four of Hearts,Three of Hearts,Two of Hearts,Ace of Hearts"}`, http.StatusCreated},
		{"haha, this is a bad test case", &model.Deck{Username: "badtest", DeckName: "bad", Deck: "", CreatedAt: time.Now().Format("2006-01-02 15:04:05"), UpdatedAt: time.Now().Format("2006-01-02 15:04:05")}, `{"status:":"not fetched"}`, http.StatusBadRequest},
	}

	for _, tc := range tt {
		t.Logf("Now running test with input: %s\n", tc.input)

		ctrl := gomock.NewController(t)
		cardsDB := mock.NewMockCardsDB(ctrl)
		cardsDB.EXPECT().Create(deckTest).AnyTimes().Return(nil)

		server = newAPIServer(cardsDB)
		server.r.Post("/cards", cardHandler)

		byteDeckTest1, err := json.Marshal(tc.input)
		assert.NoError(t, err)

		req1, err1 := http.NewRequest("POST", "/cards", bytes.NewBuffer(byteDeckTest1))
		assert.NoError(t, err1)
		req1.Header.Set("Content-Type", "application/json")

		rr1 := httptest.NewRecorder()

		server.r.ServeHTTP(rr1, req1)

		cardsDB.EXPECT().Read(tc.output.DeckName).AnyTimes().Return(tc.output, nil)

		server = newAPIServer(cardsDB)
		server.r.Get("/fetchDeck", fetchHandler)

		byteDeckTest, err := json.Marshal(tc.input)
		assert.NoError(t, err)

		req, err := http.NewRequest("GET", "/fetchDeck", bytes.NewBuffer(byteDeckTest))
		assert.NoError(t, err)
		req.Header.Set("Content-Type", "application/json")

		rr := httptest.NewRecorder()

		server.r.ServeHTTP(rr, req)
		assert.Equal(t, tc.code, rr.Code)
		assert.Equal(t, tc.response, rr.Body.String())
	}
}

func TestShuffleHandler(t *testing.T) {

	t.Logf("Now running test with input: %s\n", deckTest)

	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Create(deckTest).AnyTimes().Return(nil)

	server = newAPIServer(cardsDB)
	server.r.Post("/cards", cardHandler)

	byteDeckTest, err := json.Marshal(deckTest)
	assert.NoError(t, err)

	req, err := http.NewRequest("POST", "/cards", bytes.NewBuffer(byteDeckTest))
	assert.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	server.r.ServeHTTP(rr, req)

	cardsDB.EXPECT().Read(deckTest.DeckName).AnyTimes().Return(deckTest, nil)
	cardsDB.EXPECT().Update(gomock.Any()).AnyTimes().Return(deckTest, nil)

	server := newAPIServer(cardsDB)
	server.db.Create(deckTest)
	server.r.Post("/shuffle", shuffleHandler)

	byteDeckTest1, err := json.Marshal(deckTest)
	assert.NoError(t, err)

	req1, err := http.NewRequest("POST", "/shuffle", bytes.NewBuffer(byteDeckTest1))
	assert.NoError(t, err)
	req1.Header.Set("Content-Type", "application/json")

	rr1 := httptest.NewRecorder()

	server.r.ServeHTTP(rr1, req1)
	assert.Equal(t, http.StatusCreated, rr1.Code)

	assert.Equal(t, `{"status:":"shuffled"}`, rr1.Body.String())
}

func TestShuffleHandlerBadRequest(t *testing.T) {

	t.Logf("Now running test with input: bad test")

	ctrl := gomock.NewController(t)

	cardsDB := mock.NewMockCardsDB(ctrl)
	cardsDB.EXPECT().Update(gomock.Any()).AnyTimes().Return(deckTest, nil)

	server := newAPIServer(cardsDB)
	server.r.Post("/shuffle", shuffleHandler)

	byteDeckTest, err := json.Marshal(`{"yolo":"bad test"}`)
	assert.NoError(t, err)

	req, err := http.NewRequest("POST", "/shuffle", bytes.NewBuffer(byteDeckTest))
	assert.NoError(t, err)
	req.Header.Set("Content-Type", "application/json")

	rr := httptest.NewRecorder()

	server.r.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusBadRequest, rr.Code)

	assert.Equal(t, `{"status:":"not shuffled"}`, rr.Body.String())
}

func TestDeleteDeckHandler(t *testing.T) {

	tt := []struct {
		input    interface{}
		output   *model.Deck
		response string
		code     int
	}{
		{deckTest, deckTest, `{"status:":"deleted"}`, http.StatusCreated},
		{"haha, this is a bad test case", &model.Deck{Username: "badtest", DeckName: "bad", Deck: "", CreatedAt: time.Now().Format("2006-01-02 15:04:05"), UpdatedAt: time.Now().Format("2006-01-02 15:04:05")}, `{"status:":"not deleted"}`, http.StatusBadRequest},
	}

	for _, tc := range tt {
		t.Logf("Now running test with input: %s\n", tc.input)

		ctrl := gomock.NewController(t)

		cardsDB := mock.NewMockCardsDB(ctrl)
		cardsDB.EXPECT().Create(gomock.Any()).AnyTimes().Return(nil)
		cardsDB.EXPECT().Delete(tc.output.Username, tc.output.DeckName).AnyTimes().Return(nil)

		server.db.Create(tc.output)

		server = newAPIServer(cardsDB)
		server.r.Delete("/deleteDeck", deleteDeckHandler)

		byteDeckTest, err := json.Marshal(tc.input)
		assert.NoError(t, err)

		req, err := http.NewRequest("DELETE", "/deleteDeck", bytes.NewBuffer(byteDeckTest))
		assert.NoError(t, err)
		req.Header.Set("Content-Type", "application/json")

		rr := httptest.NewRecorder()

		server.r.ServeHTTP(rr, req)
		assert.Equal(t, tc.code, rr.Code)

		assert.Equal(t, tc.response, rr.Body.String())
	}
}
