package db

import (
	"context"
	"database/sql"
	"log"
	"time"

	"cards-app-2.0/graph/model"
)

//go:generate mockgen --source=db/db_funcs.go --destination=mocks/cardsDB_mock.go --package=mocks
type CardsDB interface {
	Create(d *model.Deck) error
	Read(deck_name string) (*model.Deck, error)
	Update(d *model.Deck) (*model.Deck, error)
	Delete(userName string, deckName string) error
}

type cardsDB struct {
	db *sql.DB
}

func NewCardsConnection(db *sql.DB) CardsDB {
	return &cardsDB{db: db}
}

func (cc *cardsDB) Create(d *model.Deck) error {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "INSERT INTO cards(username, deck_name, deck, created_at, updated_at) values(?,?,?,?,?)"

	stmt, err := cc.db.PrepareContext(ctx, query)
	if err != nil {
		log.Printf("Failed cc.db.PrepareContext in Create(): %s", err.Error())
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, d.Username, d.DeckName, d.Deck, d.CreatedAt, d.UpdatedAt)

	return err
}

func (cc *cardsDB) Read(deckName string) (*model.Deck, error) {
	ctx, cancel := context.WithTimeout(context.TODO(), 2*time.Second)
	defer cancel()

	d := &model.Deck{}
	log.Println("printing d0...", d)

	err := cc.db.QueryRowContext(ctx, "SELECT * FROM cards WHERE deck_name = ?", deckName).Scan(&d.Username, &d.DeckName, &d.Deck, &d.CreatedAt, &d.UpdatedAt)
	if err != nil {
		log.Println("printing dError...", d)
		return nil, err
	}

	log.Println("printing d1...", d)
	return d, err
}

func (cc *cardsDB) Update(d *model.Deck) (*model.Deck, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "UPDATE cards SET updated_at=?, deck=? WHERE username=? AND deck_name=?"

	stmt, err := cc.db.PrepareContext(ctx, query)
	if err != nil {
		return d, err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, time.Now().Format("2006-01-02 15:04:05"), d.Deck, d.Username, d.DeckName)
	if err != nil {
		return d, err
	}

	updatedDeck, err := cc.Read(d.DeckName)
	if err != nil {
		return nil, err
	}

	return updatedDeck, err
}

func (cc *cardsDB) Delete(userName string, deckName string) error {

	log.Printf("Running db Delete with params: %s | %s", userName, deckName)

	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	query := "DELETE FROM cards WHERE deck_name=? AND username=?"

	stmt, err := cc.db.PrepareContext(ctx, query)
	if err != nil {
		log.Printf("Failed running db.PrepareContext: %s | %s", query, err.Error())
		return err
	}
	defer stmt.Close()

	_, err = stmt.ExecContext(ctx, deckName, userName)
	if err != nil {
		log.Printf("Failed running stmt.ExecContext: %s | %s | %s", deckName, userName, err.Error())
		return err
	}

	log.Printf("Delete successful: %s | %s", userName, deckName)
	return err
}
