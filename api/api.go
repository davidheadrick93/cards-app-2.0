package api

import (
	"encoding/json"
	"log"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"cards-app-2.0/db"
	"cards-app-2.0/graph/model"
	"github.com/go-chi/chi"
)

func Routes(db db.CardsDB) chi.Router {
	server = newAPIServer(db)

	server.r.Get("/", slashHandler)
	server.r.Get("/fetchDeck", fetchHandler)
	server.r.Post("/cards", cardHandler)
	server.r.Post("/shuffle", shuffleHandler)
	server.r.Delete("/deleteDeck", deleteDeckHandler)

	return server.r
}

type deck []string

type Cards struct {
	Deck     []string `json:"deck"`
	DeckName string   `json:"deckName"`
}

func slashHandler(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Hello There!"))
}

func cardHandler(w http.ResponseWriter, r *http.Request) {
	payload := map[string]string{}

	var d *model.Deck
	decoder := json.NewDecoder(r.Body).Decode(&d)
	if decoder != nil {
		log.Printf("Failed to in cardHandler decode r.body into d: %s | %s", r.Body, d)
		payload["status:"] = "not created"
		respondWithJSON(w, http.StatusBadRequest, payload)
	} else {
		d.Username = "DavidHeadrick"
		d.Deck = newDeck().toString()
		// d.CreatedAt = time.Now()	 //ignoring for now
		// d.UpdatedAt = time.Now()  //ignoring for now
		err := server.db.Create(d)
		if err != nil {
			log.Printf("Failed in cardHandler to create d: %s | %s", d, err.Error())
			payload["error:"] = err.Error()
			respondWithJSON(w, http.StatusCreated, payload)
		} else {
			payload["status:"] = "created"
			respondWithJSON(w, http.StatusCreated, payload)
		}
	}

}

func fetchHandler(w http.ResponseWriter, r *http.Request) {
	payload := map[string]string{}

	var d *model.Deck
	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		payload["status:"] = "not fetched"
		respondWithJSON(w, http.StatusBadRequest, payload)
	} else {
		d.Username = "DavidHeadrick"
		responseDeck, err := server.db.Read(d.DeckName)
		if err != nil {
			payload["status:"] = err.Error()
			respondWithJSON(w, http.StatusInternalServerError, payload)
		} else {
			payload["status:"] = responseDeck.Deck
			respondWithJSON(w, http.StatusCreated, payload)
		}
	}
}

func shuffleHandler(w http.ResponseWriter, r *http.Request) {
	payload := map[string]string{}

	var d *model.Deck

	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		payload["status:"] = "not shuffled"
		respondWithJSON(w, http.StatusBadRequest, payload)
	} else {
		d.Username = "DavidHeadrick"
		fetchedDeck, err := server.db.Read(d.DeckName)
		if err != nil {
			payload["status:"] = err.Error()
			respondWithJSON(w, http.StatusInternalServerError, payload)
		} else {
			shuffledDeck := deck(strings.Split(fetchedDeck.Deck, ","))
			shuffledDeck.Shuffle()

			d.Deck = shuffledDeck.toString()

			_, err = server.db.Update(d)
			if err != nil {
				payload["status:"] = err.Error()
				respondWithJSON(w, http.StatusInternalServerError, payload)
			} else {
				payload["status:"] = "shuffled"
				respondWithJSON(w, http.StatusCreated, payload)
			}
		}
	}

}

func deleteDeckHandler(w http.ResponseWriter, r *http.Request) {
	payload := map[string]string{}

	var d *model.Deck
	log.Println("Initializing var d in deleteDeckHandler: ", d)

	err := json.NewDecoder(r.Body).Decode(&d)
	if err != nil {
		payload["status:"] = "not deleted"
		log.Printf("Failed running json.NewDecoder: %s", err.Error())
		respondWithJSON(w, http.StatusBadRequest, payload)
	} else {
		d.Username = "DavidHeadrick"
		err := server.db.Delete(d.Username, d.DeckName)
		if err != nil {
			payload["error:"] = err.Error()
			log.Printf("Failed running server.db.Delete: %s", err.Error())
			respondWithJSON(w, http.StatusInternalServerError, payload)
		} else {
			payload["status:"] = "deleted"
			log.Printf("deleteDeckHandler successful: %s | %s", d, err)
			respondWithJSON(w, http.StatusCreated, payload)
		}
	}
}

func respondWithJSON(w http.ResponseWriter, statusCode int, jsonPayload interface{}) {
	message, _ := json.Marshal(jsonPayload)
	w.WriteHeader(statusCode)
	w.Write(message)
}

func newDeck() deck {
	d := deck{"Joker1", "Joker2"}
	cardSuits := []string{"Spades", "Diamonds", "Clubs", "Hearts"}
	cardValues := []string{"Ace", "Two", "Three", "Four", "Five",
		"Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"}

	for i := 0; i < 2; i++ {
		for _, values := range cardValues {
			d = append(d, values+" of "+cardSuits[i])
		}
	}

	for i := 2; i < 4; i++ {
		for j := 12; j >= 0; j-- {
			d = append(d, cardValues[j]+" of "+cardSuits[i])
		}
	}

	return d
}

func (d deck) Shuffle() {
	source := rand.NewSource(time.Now().UnixNano())
	r := rand.New(source)

	for i := range d {
		newPosition := r.Intn(len(d) - 1)
		d[i], d[newPosition] = d[newPosition], d[i]
	}
}

func (d deck) toString() string {
	return strings.Join([]string(d), ",")
}
