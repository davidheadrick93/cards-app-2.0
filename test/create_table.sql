CREATE TABLE `cards` (
        `username` VARCHAR(64),
        `deck_name` VARCHAR(64) PRIMARY KEY,
        `deck` VARCHAR(256),
        `created_at` VARCHAR(64),
        `updated_at` VARCHAR(64)  
    );