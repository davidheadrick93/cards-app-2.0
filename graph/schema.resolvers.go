package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"strings"
	"time"

	"cards-app-2.0/graph/generated"
	"cards-app-2.0/graph/model"
	"cards-app-2.0/helper"
)

func (r *mutationResolver) CreateDeck(ctx context.Context, input string) (*model.Deck, error) {

	d := model.Deck{
		Username:  "DavidHeadrick",
		DeckName:  input,
		Deck:      helper.NewDeck().ToString(),
		CreatedAt: time.Now().Format("2006-01-02 15:04:05"),
		UpdatedAt: time.Now().Format("2006-01-02 15:04:05"),
	}

	return &d, r.DB.Create(&d)
}

func (r *mutationResolver) ShuffleDeck(ctx context.Context, input string) (*model.Deck, error) {
	d, _ := r.DB.Read(input)

	tempDeck := helper.Deck(strings.Split(d.Deck, ","))
	tempDeck.Shuffle()
	shuffledDeck := tempDeck.ToString()

	d.Deck = shuffledDeck

	updatedDeck, err := r.DB.Update(d)

	return updatedDeck, err
}

func (r *mutationResolver) DeleteDeck(ctx context.Context, input string) (string, error) {

	return "deck successfully deleted!", r.DB.Delete("DavidHeadrick", input)
}

func (r *queryResolver) GetDeckDetails(ctx context.Context, input string) (*model.Deck, error) {
	d, err := r.DB.Read(input)

	return d, err
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
