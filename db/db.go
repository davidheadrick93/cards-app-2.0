package db

import (
	"database/sql"
	"log"

	_ "github.com/mattn/go-sqlite3"
)

func Connect(fileName string) *sql.DB {

	log.Println("Opening DB file..." + fileName)
	log.Printf("Opening DB file...%s", fileName)
	log.Println("Printing DB filename on next line...")
	log.Println(fileName)

	db, err := sql.Open("sqlite3", fileName)
	if err != nil {
		log.Fatalf("Error opening the specified database file: %s | Error test: %s", fileName, err.Error())
	}

	return db
}
